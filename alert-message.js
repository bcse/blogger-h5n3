var urlParams = {};
(function () {
  var e,
    a = /\+/g,  // Regex for replacing addition symbol with a space
    r = /([^&=]+)=?([^&]*)/g,
    d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
    q = window.location.search.substring(1);

  while (e = r.exec(q))
    urlParams[d(e[1])] = d(e[2]);
})();
$(function(){
  if ('notfound' in urlParams) {
    $('#alert-message').removeClass('warning error success info').addClass('alert-message block-message error').html('<a class="close" href="#">×</a><p><strong>您要找的頁面可能已經刪除。</strong>請試著使用左下方的搜尋功能再次尋找，或者檢視 Google 提供的<a href="http://www.google.com/search?q=' + encodeURIComponent('cache:http://blog.bcse.tw' + urlParams['notfound']) + '" rel="external">頁庫存檔</a>。</p>');
  }
});